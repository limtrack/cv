export const FA_SSNN = {
  facebook: 'facebook-f',
  instagram: 'instagram',
  linkedin: 'linkedin-in',
  pinterest: 'pinterest-p',
  tumblr: 'tumblr',
  twitter: 'twitter',
  skype: 'skype',
  youtube: 'youtube',
};

export const SCROLLER_MSECONDS = 2000;
