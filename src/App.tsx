import React from 'react';
// Context
import { ContextProvider } from './provider/components/Context';
// Bootstrap
import Container from 'react-bootstrap/Container';
// Components
import SelectLanguage from './components/elements/SelectLanguage';
import FloatingSocialNetworks from './components/elements/FloatingSocialNetworks';
import NavigatorArrow from './components/elements/NavigatorArrow';
import SectionsRender from './components/features/SectionsRender';
import CustomModal from './components/elements/CustomModal';
// FontAwesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import {
  faChevronDown,
  faChevronUp,
  faChevronLeft,
  faChevronRight,
} from '@fortawesome/free-solid-svg-icons';

library.add(fab, faChevronDown, faChevronUp, faChevronLeft, faChevronRight);

const App: React.FC = (): JSX.Element => {
  return (
    <ContextProvider>
      {/* Modal */}
      <CustomModal />
      {/* Select language */}
      <SelectLanguage />
      {/* Social networks */}
      <FloatingSocialNetworks />
      <Container>
        {/* Sections */}
        <SectionsRender />
        {/* Navigator */}
        <NavigatorArrow />
      </Container>
    </ContextProvider>
  );
};

export default App;
