import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import App from './App';

describe('Testing App works correctly', () => {
  it.only('Testing differents sections and default languages', async () => {
    const { findByTestId, findByText } = render(<App />);
    // Navigation arrow
    const navigatorArrow = await findByTestId('navigation-arrow');
    // Selector language
    const selectLanguage = await findByTestId('select-language');
    // Section tests in spanish
    const sectionTextsEs = [
      'React, Vue, Backbone, CSS, HTML...',
      'Experiencia',
      'Estudios',
      'Portfolio',
      'Otros',
      'Contacto',
    ];
    // Section tests in english
    const sectionTextsEn = [
      'React, Vue, Backbone, CSS, HTML...',
      'Experience',
      'Studies',
      'Portfolio',
      'Others',
      'Contact',
    ];

    // All sections in Spanish
    for await (const text of sectionTextsEs) {
      const elementByText = await findByText(text);
      expect(elementByText).toBeInTheDocument();
      fireEvent.click(navigatorArrow);
    }

    // Select English language
    fireEvent.change(selectLanguage, { target: { value: 'en' } });

    // All sections in English
    for await (const text of sectionTextsEn) {
      const elementByText = await findByText(text);
      expect(elementByText).toBeInTheDocument();
      fireEvent.click(navigatorArrow);
    }
  });
});
