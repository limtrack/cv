import messages from './messages';

const get = require('lodash.get');

export const getMessageByPath = (lang: string, path: string): string | any => {
  return get(messages, `${lang}.${path}`, '');
};
