const messages = {
  es: {
    elements: {
      knowMeFinished: {
        text:
          '<h4>Hasta aquí mi CV online, ahora tienes 2 opciones</h4>' +
          '<ol>' +
          '<li>' +
          '<b>Eres un empresa o similar:</b> Quieres ofrecerme algún trabajo ' +
          'entonces puedes <a href="/doc/cv_es.pdf">descargar mi CV</a> en PDF si lo deseas y ' +
          'contactar conmigo.' +
          '<br />' +
          '<br />' +
          '</li>' +
          '<li>' +
          '<b>Eres un particular:</b> Si te ha molado el CV online puedes ' +
          '<a href="https://bitbucket.org/limtrack/cv/src/master/" target="_blank">descargarlo y modicarlo</a> fácilmente, es libre puede ' +
          'hacer con él lo que quieras.' +
          '</li>' +
          '</ol>' +
          '<p>Sin nada más que comentar, recibe un cordial saludo de mi parte.</p>',
      },
      modal: {
        titleKnowMeFinished: 'Gracias por el interés',
      },
    },
    sections: {
      information: {
        hello: 'Hola, ',
        name: 'soy Antonio Irlandés García',
        job: ['Programador Frontend'],
        description: 'React, Vue, Backbone, CSS, HTML...',
        buttonKnowMe: 'Conóceme',
        buttonDownloadCV: 'Descargar CV',
        imgAlt: 'Antonio Irlandés García',
        img: '/images/profile_es.png',
        cvURL: '/doc/cv_es.pdf',
      },
      experiences: {
        title: 'Experiencia',
        subtitle: 'Vida laboral',
        data: [
          {
            date: 'Noviembre 2018 - Noviembre 2019',
            title: 'Geographica (Carto)',
            subtitle: 'Programador senior Frontend',
            description:
              '<ul>' +
              '<li>' +
              'Desarrollo del producto URBO (ciudades inteligentes) de Telefónica. Software ' +
              'indicado para la gestión en las admistraciones públicas de los recursos que ' +
              'una ciudad posee (basuras, iluminación, patrimonio...)' +
              '</li>' +
              '<li>' +
              'AquaGIS (PWA) para gestionar las incidencias, a pie de calle, de un ' +
              'operario de una "empresa de aguas".' +
              '</li>' +
              '<li>' +
              'Breve participación en proyectos como para RealMadrid de ' +
              'Baloncesto (aplicación para seguir online los partidos) y el proyecto URBO2 ' +
              '(continuación del proyecto URBO, re-escrito y re-diseñado bajo Angular).' +
              '</li>' +
              '</ul>',
            tags: [
              'Angular4',
              'Vue',
              'Backbone',
              'Underscore',
              'HTML5',
              'CSS',
              'Bootstrap',
              'CartoDB',
              'Mapbox',
              'Git',
              'D3',
            ],
          },
          {
            date: 'Abril 2018 - Octubre 2018',
            title: 'KairosDS',
            subtitle: 'Programador senior Frontend',
            description:
              '<ul>' +
              '<li>' +
              'Miembro del equipo de desarrollo de “SimpleKYC” (Australia), aplicación ' +
              'usada para la contratación de diversos productos financieros asociados a ' +
              'American Express.' +
              '</li>' +
              '<li>' +
              'Breve participación en el desarrollo del software interno de la compañia para la ' +
              'gestión de las actividades del departamento de RRHH.' +
              '</li>' +
              '</ul>',
            tags: ['Angular4', 'Vue', 'Git', 'Scrum', 'HTML5', 'CSS'],
          },
          {
            date: 'Julio 2016 - Abril 2018',
            title: 'BBVA (Babel- Indra)',
            subtitle: 'Programador senior Frontend',
            description:
              '<ul>' +
              '<li>' +
              'Participación en el proyecto de "Transformación digital" (portal web). ' +
              'Desarrolle mi trabajo en 3 equipos distintos:' +
              '<ul>' +
              '<li>' +
              'Pagos digitales: Participación en el producto "Bizum" (cliente para BBVA).' +
              '</li>' +
              '<li>' +
              'Seguros: Participación en el producto "Plan EstarSeguro."' +
              '</li>' +
              '<li>' +
              'Alta/Contratación: Participación en el producto "Alta conjunta de 2 clientes" ' +
              '(presentación del producto en el edificio "la Vela").' +
              '</li>' +
              '</ul>' +
              '</li>' +
              '</ul>',
            tags: ['Backbone', 'Underscore', 'Vue', 'Scrum', 'Git'],
          },
          {
            date: 'Diciembre 2014 - Julio 2016',
            title: 'Betaminders',
            subtitle: 'Programador Frontend & CTO',
            description:
              '<ul>' +
              '<li>' +
              'Factoría de software para el desarrollo de aplicaciónes web / móviles ' +
              '(híbridas) para los distintos clientes de la compañía.' +
              '</li>' +
              '<li>' +
              'Algunos proyectos:' +
              '<ul>' +
              '<li>' +
              'DentalHorse: Aplicación par dentistas de caballos.' +
              '</li>' +
              '<li>' +
              'Footters: Red social para equipos de fútbol base.' +
              '</li>' +
              '<li>' +
              'Spycy: Aplicación para medir el impacto de perfiles de instagram' +
              'sobre sus seguidores.' +
              '</li>' +
              '</ul>' +
              '</li>' +
              '</ul>',
            tags: [
              'Backbone',
              'Underscore',
              'RequireJS',
              'Polymer',
              'Cordova',
              'Sympony2',
              'Wordpress',
              'Scrum',
              'Git',
              'HTML5',
              'CSS',
            ],
          },
          {
            date: 'Enero 2013 - Diciembre 2014',
            title: 'Iventiajobs',
            subtitle: 'Programador Frontend',
            description:
              '<p> Miembro del equipo de desarrollo de las "startups" Iventiajobs.com y Cornerfy.com.</p>',
            tags: [
              'Symfony2',
              'JavaScript',
              'HTML5',
              'CSS',
              'Bootstrap',
              'Git',
            ],
          },
          {
            date: 'Noviembre 2010 - Noviembre 2011',
            title: 'Tengoentradas.com',
            subtitle: 'Analista programador',
            description:
              '<p> Desarrollo y mantenimiento del portal tengoentradas.com así de como todas ' +
              'las herramientas asociadas a este.</p>',
            tags: ['CakePHP', 'osCommerce', 'JavaScript', 'HTML', 'CSS'],
          },
          {
            date: 'Diciembre 2007 - Diciembre 2010',
            title: 'Isotrol',
            subtitle: 'Programador',
            description:
              '<ul>' +
              '<li>' +
              'Miembro del equipo de desarrollo del CGA (Junta de Andalucía).' +
              '</li>' +
              '<li>' +
              'Desarrollo del software de gestión de incidencias para gestionar ' +
              'los centros educativos en Andalucía.' +
              '</li>' +
              '<li>' +
              'Mantenimiento del software online instalado en los servidores ' +
              'de cada centro educativo.' +
              '</li>' +
              '</ul>',
            tags: [
              'LAMP',
              'Javascript',
              'HTML',
              'CSS',
              'Wordpress',
              'Moodle',
              'MediaWiki',
            ],
          },
          {
            date: 'Junio 2006 - Diciembre 2007',
            title: 'H2e',
            subtitle: 'Analista programador',
            description:
              '<p>' +
              'Desarrollo de software online para los distintos clientes (gestión de ' +
              'proyectos y tratamiento con clientes) que la agencia poseía. Factoría de ' +
              'Software.' +
              '</p>',
            tags: ['LAMP', 'JavaScript', 'HTML', 'CSS'],
          },
        ],
      },
      studies: {
        title: 'Estudios',
        subtitle: 'Vida académica',
        data: [
          {
            date: '2006-2006',
            title: 'Ingenieria técnica informática de sistemas',
            description: 'Universidad de Sevilla (Estudios inacabados)',
          },
          {
            date: '2004-2006',
            title: 'Administración de sistemas informáticos',
            description: 'IES Lucus Solis - Sanlucar la Mayor (Sevilla)',
          },
        ],
      },
      works: {
        title: 'Portfolio',
        subtitle: 'Algo que enseñar',
        data: [
          {
            name: 'CV',
            description:
              'Un CV online para que puedas mostrar al mundo tu perfil más profesional',
            img: '/images/work_cv.jpg',
            url: 'http://www.programador-frontend.com/',
            code: 'https://bitbucket.org/limtrack/cv/src/master/',
            tags: ['React', 'Bootstrap'],
          },
          {
            name: 'Driving Map',
            description:
              'Un minijuego online de carreras (no es lo que tú piensas) con los personajes de Mario Kart',
            img: '/images/work_drivingmap.jpg',
            url: 'http://drivingmap.programador-frontend.com/',
            code: 'https://bitbucket.org/limtrack/drivingmap/src/master/',
            tags: ['React', 'Redux', 'Bootstrap', 'GoogleMaps', 'Firebase'],
          },
          {
            name: 'Testing RxJS',
            description:
              'Una pequeña aplicación desarrollada en React para probar la programación reactiva (RxJS)',
            img: '/images/work_rxjs.jpg',
            url: 'http://rxjs.programador-frontend.com/',
            code: 'https://bitbucket.org/limtrack/rxjs/src/master/',
            tags: ['React', 'Webpack', 'RxJS', 'Ant Design'],
          },
        ],
      },
      others: {
        title: 'Otros',
        subtitle: 'A destacar',
        data: [
          {
            date: '2019',
            title: 'Minerva (Aceleradora)',
            description:
              'Proyecto "Taqtil", kiosco expendedor de ofertas en los centros comerciales.',
          },
          {
            date: '2013',
            title: 'Yuzz (Aceleradora)',
            description:
              'Proyecto "Chusyu", plataforma que agrupa a usuarios con los mismos deseos de compra e informa a las tiendas generando ofertas sobre estos deseos',
          },
        ],
      },
      contact: {
        title: 'Contacto',
        subtitle: 'Algunos datos más',
        data: [
          {
            id: 'Nombre',
            value: 'Antonio Irlandés García',
          },
          {
            id: 'Fecha de nacimiento',
            value: '31 de octubre de 1982',
          },
          {
            id: 'Profesión',
            value: 'Programador Frontend',
          },
          {
            id: 'Dirección',
            value: 'Calle Pintor Mendoza 39, 13300 Valdepeñas (Ciudad Real)',
          },
          {
            id: 'Teléfono',
            value: '662 03 38 15',
          },
          {
            id: 'Email',
            value: 'limtrack@gmail.com',
          },
        ],
      },
    },
  },
  en: {
    elements: {
      knowMeFinished: {
        text:
          '<h4>To here mi online CV, now you have two options</h4>' +
          '<ol>' +
          '<li>' +
          '<b>You are a company or similar:</b> You want offers to me some job ' +
          'then you can <a href="/doc/cv_en.pdf">download my CV</a> like PDF file and you can ' +
          'contact with me.' +
          '<br />' +
          '<br />' +
          '</li>' +
          '<li>' +
          '<b>You are a private:</b> If you think that these online CV is awesome (kind of) you can ' +
          '<a href="https://bitbucket.org/limtrack/cv/src/master/" target="_blank"> download and modify</a> easily, it is free  and you can ' +
          'make with it everything that you want.' +
          '</li>' +
          '</ol>' +
          '<p>Nothing more to say you, receives a warm greeting from me.</p>',
      },
      modal: {
        titleKnowMeFinished: 'Thank you for watching',
      },
    },
    sections: {
      information: {
        hello: 'Hello, ',
        name: "I'm Antonio Irlandés García",
        job: 'Frontend Developer',
        description: 'React, Vue, Backbone, CSS, HTML...',
        buttonKnowMe: 'Knonw me',
        buttonDownloadCV: 'Download CV',
        imgAlt: 'Antonio Irlandés García',
        img: '/images/profile_en.png',
        cvURL: '/doc/cv_en.pdf',
      },
      experiences: {
        title: 'Experience',
        subtitle: 'Job life',
        data: [
          {
            date: 'November 2018 - November 2019',
            title: 'Geographica (Carto)',
            subtitle: 'Senior programmer Frontend',
            description:
              '<ul>' +
              '<li>' +
              'Product development URBO (smart cities) of Telefónica. Software ' +
              'thought to manage the resources (garbage, lighting, heritache...) by the differents public administrations. ' +
              '</li>' +
              '<li>' +
              'AquaGIS (PWA) manages the issues "in situ" that one "water company" operator.' +
              '</li>' +
              '<li>' +
              'Brief participation in other projects like Basketball RealMadrid and the URBO2 project ' +
              '(continuation to URBO)' +
              '</li>' +
              '</ul>',
            tags: [
              'Angular4',
              'Vue',
              'Backbone',
              'Underscore',
              'HTML5',
              'CSS',
              'Bootstrap',
              'CartoDB',
              'Mapbox',
              'Git',
              'D3',
            ],
          },
          {
            date: 'April 2018 - October 2018',
            title: 'KairosDS',
            subtitle: 'Senior programmer Frontend',
            description:
              '<ul>' +
              '<li>' +
              'Member of the "SimpleKYC" develop team (Australia), application to contract differents ' +
              'American Express products.' +
              '</li>' +
              '<li>' +
              'Brief participation in a internal software company to manage the HHRR department.' +
              '</li>' +
              '</ul>',
            tags: ['Angular4', 'Vue', 'Git', 'Scrum', 'HTML5', 'CSS'],
          },
          {
            date: 'July 2016 - April 2018',
            title: 'BBVA (Babel- Indra)',
            subtitle: 'Senior programmer Frontend',
            description:
              '<ul>' +
              '<li>' +
              'Member of the "Transformación digital" developer team (web). ' +
              'I was partipating in 3 teams:' +
              '<ul>' +
              '<li>' +
              'Digital payments: "Bizum" (client to BBVA).' +
              '</li>' +
              '<li>' +
              'Insurances: "Plan EstarSeguro" product.' +
              '</li>' +
              '<li>' +
              'SingIn/Engagement: "Alta conjunta de 2 clientes" product.' +
              '</li>' +
              '</ul>' +
              '</li>' +
              '</ul>',
            tags: ['Backbone', 'Underscore', 'Vue', 'Scrum', 'Git'],
          },
          {
            date: 'December 2014 - July 2016',
            title: 'Betaminders',
            subtitle: 'Frontend Development & CTO',
            description:
              '<ul>' +
              '<li>' +
              'Software factory for the development of web / mobiles applications' +
              "(hybrid) for the different company's clients." +
              '</li>' +
              '<li>' +
              'Some projects:' +
              '<ul>' +
              '<li>' +
              'DentalHorse: App (hybrid) for horses dentist.' +
              '</li>' +
              '<li>' +
              'Footters: Social network to base football team.' +
              '</li>' +
              '<li>' +
              'Spycy: App to get the social impact between differents profiles and followers.' +
              '</li>' +
              '</ul>' +
              '</li>' +
              '</ul>',
            tags: [
              'Backbone',
              'Underscore',
              'RequireJS',
              'Polymer',
              'Cordova',
              'Sympony2',
              'Wordpress',
              'Scrum',
              'Git',
              'HTML5',
              'CSS',
            ],
          },
          {
            date: 'January 2013 - December 2014',
            title: 'Iventiajobs',
            subtitle: 'Frontend Development',
            description:
              '<p> Member of development team (Frontend) of "startups" Iventiajobs.com and Cornerfy.com.</p>',
            tags: [
              'Symfony2',
              'JavaScript',
              'HTML5',
              'CSS',
              'Bootstrap',
              'Git',
            ],
          },
          {
            date: 'November 2010 - November 2011',
            title: 'Tengoentradas.com',
            subtitle: 'Analyst programmer',
            description:
              '<p> Development and mantenance of tengoentradas.com web and other tools associated to it.</p>',
            tags: ['CakePHP', 'osCommerce', 'JavaScript', 'HTML', 'CSS'],
          },
          {
            date: 'December 2007 - December 2010',
            title: 'Isotrol',
            subtitle: 'Programmer',
            description:
              '<ul>' +
              '<li>' +
              'Member of development team of CGA (Junta de Andalucía).' +
              '</li>' +
              '<li>' +
              'Development software to manage the differents issues education centers from Andalucia.' +
              '</li>' +
              '<li>' +
              'I support to the software installed in the servers of education centers.' +
              '</li>' +
              '</ul>',
            tags: [
              'LAMP',
              'Javascript',
              'HTML',
              'CSS',
              'Wordpress',
              'Moodle',
              'MediaWiki',
            ],
          },
          {
            date: 'June 2006 - December 2007',
            title: 'H2e',
            subtitle: 'Analyst programmer',
            description:
              '<p>' +
              'Development online software to the differents company clientes ' +
              'Software factory' +
              '</p>',
            tags: ['LAMP', 'JavaScript', 'HTML', 'CSS'],
          },
        ],
      },
      studies: {
        title: 'Studies',
        subtitle: 'Academic life',
        data: [
          {
            date: '2006-2006',
            title: 'Computer systems technical engineering',
            description: 'Sevilla University (unfinished studies)',
          },
          {
            date: '2004-2006',
            title: 'Computer systems administration',
            description:
              'High School Lucus Solis - Sanlucar la Mayor (Sevilla)',
          },
        ],
      },
      works: {
        title: 'Portfolio',
        subtitle: 'Something to show',
        data: [
          {
            name: 'CV',
            description:
              'A online CV for you can show to the world your proffesional profile',
            img: '/images/work_cv.jpg',
            url: 'http://www.programador-frontend.com/',
            code: 'https://bitbucket.org/limtrack/cv/src/master/',
            tags: ['React', 'Bootstrap'],
          },
          {
            name: 'Driving Map',
            description:
              'A minigame of online race (it does not exactly that you think)  with the "Mario Karts" characters',
            img: '/images/work_drivingmap.jpg',
            url: 'http://drivingmap.programador-frontend.com/',
            code: 'https://bitbucket.org/limtrack/drivingmap/src/master/',
            tags: ['React', 'Redux', 'Bootstrap', 'GoogleMaps', 'Firebase'],
          },
          {
            name: 'Testing RxJS',
            description:
              'A small React application to test reactive programming (RxJS)',
            img: '/images/work_rxjs.jpg',
            url: 'http://rxjs.programador-frontend.com/',
            code: 'https://bitbucket.org/limtrack/rxjs/src/master/',
            tags: ['React', 'Webpack', 'RxJS', 'Ant Design'],
          },
        ],
      },
      others: {
        title: 'Others',
        subtitle: 'Highlight',
        data: [
          {
            date: '2019',
            title: 'Minerva (Bussines Angels)',
            description:
              'Project "Taqtil", a vending machine about the different offers that the shops of malls makes.',
          },
          {
            date: '2013',
            title: 'Yuzz (Bussines Angels)',
            description:
              'Project "Chusyu", platform online where potential customers and shops shares information about desire products and these last make offers about those products.',
          },
        ],
      },
      contact: {
        title: 'Contact',
        subtitle: 'Some more information',
        data: [
          {
            id: 'Name',
            value: 'Antonio Irlandés García',
          },
          {
            id: 'Birthdate',
            value: '31 de octubre de 1982',
          },
          {
            id: 'Job',
            value: 'Frontend Developer',
          },
          {
            id: 'Address',
            value: 'Street Pintor Mendoza 39, 13300 Valdepeñas (Ciudad Real)',
          },
          {
            id: 'Phone',
            value: '662 03 38 15',
          },
          {
            id: 'Email',
            value: 'limtrack@gmail.com',
          },
        ],
      },
    },
  },
};

export default messages;
