import Information from '../components/sections/Information';
import Experiences from '../components/sections/Experiences';
import Studies from '../components/sections/Studies';
import Works from '../components/sections/Works';
import Others from '../components/sections/Others';
import Contact from '../components/sections/Contact';

// Types
export type BrandType =
  | 'facebook'
  | 'instagram'
  | 'linkedin'
  | 'pinterest'
  | 'tumblr'
  | 'twitter'
  | 'skype'
  | 'youtube';

export type SSNNType = {
  id: BrandType;
  value: string;
  isUrl: boolean;
};

export type SectionsType = {
  component: any;
  id: string;
  visible: boolean;
};

export type StateType = {
  currentLanguage: string;
  languages: {
    id: string;
    label: string;
  }[];
  modal: {
    actionAcceptButton?: () => void;
    actionCancelButton?: () => void;
    backdrop?: boolean;
    title?: string;
    content: string | any;
    showHeader?: boolean;
    showFooter?: boolean;
    showAcceptButton?: boolean;
    showCancelButton?: boolean;
    showCloseButton?: boolean;
    textAcceptButton?: string;
    textCancelButton?: string;
    visible: boolean;
  };
  currentSection: string;
  sections: SectionsType[];
  ssnn: SSNNType[];
};

// Values (default)
export const initState = {
  currentLanguage: 'es',
  languages: [
    {
      id: 'es',
      label: 'Español',
    },
    {
      id: 'en',
      label: 'English',
    },
  ],
  modal: {
    backdrop: true,
    title: 'Title Modal',
    content: '',
    showHeader: true,
    showFooter: true,
    showAcceptButton: true,
    showCancelButton: true,
    showCloseButton: true,
    textAcceptButton: 'Accept',
    textCancelButton: 'Cancel',
    visible: false,
  },
  currentSection: 'information',
  sections: [
    {
      component: Information,
      id: 'information',
      visible: true,
    },
    {
      component: Experiences,
      id: 'experience',
      visible: false,
    },
    {
      component: Studies,
      id: 'studies',
      visible: false,
    },
    {
      component: Works,
      id: 'works',
      visible: false,
    },
    {
      component: Others,
      id: 'others',
      visible: false,
    },
    {
      component: Contact,
      id: 'contact',
      visible: false,
    },
  ],
  ssnn: [
    {
      id: 'twitter' as BrandType,
      value: 'https://twitter.com/developer_es',
      isUrl: true,
    },
    {
      id: 'linkedin' as BrandType,
      value: 'https://www.linkedin.com/in/antonioirlandesgarcia',
      isUrl: true,
    },
    {
      id: 'skype' as BrandType,
      value: 'developer_es',
      isUrl: false,
    },
  ],
};
