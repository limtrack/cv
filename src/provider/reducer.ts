import { initState, StateType } from './state';

const reducer = (
  state: StateType,
  action: { type: string; value: any },
): StateType => {
  switch (action.type) {
    // app
    case 'CHANGE_LANGUAGE': {
      return {
        ...state,
        currentLanguage: action.value,
      };
    }
    // sections
    case 'SET_CURRENT_SECTION': {
      return {
        ...state,
        currentSection: action.value,
        sections: state.sections.map((section: any) => {
          if (section.id === action.value) {
            section.visible = true;
          }
          return section;
        }),
      };
    }
    // modal
    case 'MODAL_UPDATE': {
      return {
        ...state,
        modal: Object.assign(initState.modal, action.value),
      };
    }
    case 'MODAL_RESET': {
      return { ...state, modal: initState.modal };
    }
    default:
      return state;
  }
};

export default reducer;
