import React, { useContext, useEffect } from 'react';
// Constants
import { SCROLLER_MSECONDS } from '../../constants';
// Context
import { Context } from '../../provider/components/Context';
import { SectionsType } from '../../provider/state';
// Components - UI
import { scroller, Element } from 'react-scroll';

const SectionsRender: React.FC = (): JSX.Element => {
  const { state } = useContext(Context);
  const visibleSections = state.sections.filter(
    (section: SectionsType) => section.visible,
  );

  // Move to section
  useEffect(() => {
    scroller.scrollTo(state.currentSection, {
      duration: SCROLLER_MSECONDS,
      delay: 0,
      smooth: 'easeInOutQuart',
    });
  }, [state.currentSection]);

  return (
    <div className="sections">
      {visibleSections.map((section: SectionsType) => {
        const Component = section.component;
        return (
          <Element name={section.id} key={section.id}>
            {typeof Component !== undefined ? <Component /> : null}
          </Element>
        );
      })}
    </div>
  );
};

export default SectionsRender;
