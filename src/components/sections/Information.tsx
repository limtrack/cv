import React, { useContext } from 'react';
// Context
import { Context } from '../../provider/components/Context';
import { SectionsType } from '../../provider/state';
// I18N
import { getMessageByPath } from '../../locale';
// UI
import Button from 'react-bootstrap/Button';
import Typewriter from 'typewriter-effect';

const Information: React.FC = (): JSX.Element => {
  const { state, dispatch } = useContext(Context);
  // get messages from locale
  const messages = (key: string): string => {
    return getMessageByPath(
      state.currentLanguage,
      `sections.information.${key}`,
    );
  };

  // Go next section
  const goNextSection = (): void => {
    const currentSectionIndex = state.sections.findIndex(
      (section: SectionsType) => {
        return section.id === state.currentSection;
      },
    );
    const nextSectionIndex =
      currentSectionIndex + 1 < state.sections.length
        ? currentSectionIndex + 1
        : 0;

    dispatch({
      type: 'SET_CURRENT_SECTION',
      value: state.sections[nextSectionIndex].id,
    });
  };

  return (
    <div className="section">
      <div className="section-content">
        <div className="information">
          <div className="information__left">
            <h2 className="information__name">
              <span className="red">{messages('hello')}</span>
              {messages('name')}
            </h2>
            <h1 className="information__job">
              <Typewriter
                options={{
                  strings: messages('job'),
                  autoStart: true,
                  loop: true,
                  delay: 250,
                  deleteSpeed: 2500,
                }}
              />
            </h1>
            <h3 className="information__job-description">
              {messages('description')}
            </h3>
            <div className="information__actions">
              <Button variant="primary" onClick={goNextSection}>
                {messages('buttonKnowMe')}
              </Button>
              <Button variant="outline-primary" href={messages('cvURL')}>
                {messages('buttonDownloadCV')}
              </Button>
            </div>
          </div>
          <div className="information__right">
            <img
              src={messages('img')}
              className="information__photo"
              alt={messages('imgAlt')}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Information;
