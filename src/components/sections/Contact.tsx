import React, { Fragment, useContext } from 'react';
// Context
import { Context } from '../../provider/components/Context';
// I18N
import { getMessageByPath } from '../../locale';
// Components - UI
import LeftToRight from '../layouts/LeftToRight';
import SocialNetworks from '../elements/SocialNetworks';

// Types
type ContactType = {
  id: string;
  value: string;
};

const Contact: React.FC = (): JSX.Element => {
  const { state } = useContext(Context);
  // get messages from locale
  const messages = (key: string): string | any => {
    return getMessageByPath(state.currentLanguage, `sections.contact.${key}`);
  };

  return (
    <LeftToRight
      name="contact"
      title={messages('title')}
      subtitle={messages('subtitle')}
      content={
        <div className="white-box appear-and-translateY__afterload-section">
          <dl className="row mb-3">
            {messages('data').map((data: ContactType) => {
              return (
                <Fragment key={data.id}>
                  <dt className="col-sm-5">{data.id}</dt>
                  <dd className="col-sm-7">{data.value}</dd>
                </Fragment>
              );
            })}
          </dl>
          <div className="contact__ssnn">
            <SocialNetworks />
          </div>
        </div>
      }
    />
  );
};

export default Contact;
