import React, { useContext } from 'react';
// Context
import { Context } from '../../provider/components/Context';
// I18N
import { getMessageByPath } from '../../locale';
// Components - UI
import Timeline from '../layouts/TimeLine';

const Experiences: React.FC = (): JSX.Element => {
  const { state } = useContext(Context);
  // get messages from locale
  const messages = (key: string): string | any => {
    return getMessageByPath(
      state.currentLanguage,
      `sections.experiences.${key}`,
    );
  };
  // every data
  const data = messages('data');

  return (
    <Timeline
      name="experiences"
      title={messages('title')}
      subtitle={messages('subtitle')}
      data={data}
    ></Timeline>
  );
};

export default Experiences;
