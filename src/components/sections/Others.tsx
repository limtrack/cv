import React, { useContext } from 'react';
// Context
import { Context } from '../../provider/components/Context';
// I18N
import { getMessageByPath } from '../../locale';
// Components - UI
import LeftToRight from '../layouts/LeftToRight';

const Others: React.FC = (): JSX.Element => {
  const { state } = useContext(Context);
  // get messages from locale
  const messages = (key: string): string | any => {
    return getMessageByPath(state.currentLanguage, `sections.others.${key}`);
  };

  return (
    <LeftToRight
      name="others"
      title={messages('title')}
      subtitle={messages('subtitle')}
      content={messages('data').map((data: any, index: number) => {
        return (
          <div
            key={index}
            className="white-box small mb-3 appear-and-translateY__afterload-section"
          >
            <h4 className="white-box__title">{data.date}</h4>
            <h5 className="white-box__subtitle">{data.title}</h5>
            <p>{data.description}</p>
          </div>
        );
      })}
    />
  );
};

export default Others;
