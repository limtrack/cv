import React, { useContext } from 'react';
// Context
import { Context } from '../../provider/components/Context';
// I18N
import { getMessageByPath } from '../../locale';
// Components - UI
import Gallery from '../layouts/Gallery';

const Works: React.FC = (): JSX.Element => {
  const { state } = useContext(Context);
  // get messages from locale
  const messages = (key: string): string | any => {
    return getMessageByPath(state.currentLanguage, `sections.works.${key}`);
  };

  return (
    <Gallery
      name="works"
      title={messages('title')}
      subtitle={messages('subtitle')}
      data={messages('data')}
    />
  );
};

export default Works;
