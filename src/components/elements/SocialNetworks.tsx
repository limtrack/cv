import React, { useContext } from 'react';
// Context & State
import { Context } from '../../provider/components/Context';
import { SSNNType } from '../../provider/state';
// Contants
import { FA_SSNN } from '../../constants';
// Bootstrap
import Button from 'react-bootstrap/Button';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
// FontAwesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const SocialNetworks: React.FC = (): JSX.Element => {
  // State
  const { state } = useContext(Context);
  // Navigate to social network URL
  const navigateToSSNN = (sn: SSNNType): void => {
    if (sn.isUrl && sn.value) {
      window.location.href = sn.value;
    }
  };

  return (
    <div className="ssnn">
      {state.ssnn.map((sn: SSNNType) => {
        const faIcon: any = FA_SSNN[sn.id as keyof typeof FA_SSNN];

        return sn.isUrl ? (
          <Button
            key={sn.id}
            className={`btn-circle btn-${sn.id}`}
            onClick={navigateToSSNN.bind(null, sn)}
          >
            <FontAwesomeIcon
              icon={['fab', faIcon]}
              className="btn-circle-icon"
            />
          </Button>
        ) : (
          <OverlayTrigger
            key={sn.id}
            placement="bottom"
            overlay={<Tooltip id="tooltip-bottom">{sn.value}</Tooltip>}
          >
            <Button className={`btn-circle btn-${sn.id}`}>
              <FontAwesomeIcon
                icon={['fab', faIcon]}
                className="btn-circle-icon"
              />
            </Button>
          </OverlayTrigger>
        );
      })}
    </div>
  );
};

export default SocialNetworks;
