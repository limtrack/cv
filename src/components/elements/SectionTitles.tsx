import React, { Fragment } from 'react';

type Props = {
  title?: string;
  subtitle?: string;
};

const SectionTitles: React.FC<Props> = ({
  title,
  subtitle,
}: Props): JSX.Element => {
  return (
    <Fragment>
      {title ? (
        <h2
          data-testid="title"
          className="section__title appear-and-translateY__section_title"
        >
          {title}
        </h2>
      ) : null}
      {subtitle ? (
        <h3
          data-testid="subtitle"
          className="section__subtitle appear-and-translateY__section_subtitle"
        >
          {subtitle}
        </h3>
      ) : null}
    </Fragment>
  );
};

export default SectionTitles;
