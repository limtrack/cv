import React, { useContext, useState } from 'react';
// Context
import { Context } from '../../provider/components/Context';
import { SectionsType } from '../../provider/state';
// I18N
import { getMessageByPath } from '../../locale';
// Components - UI
import KnowMeFinished from './KnowMeFinished';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const debounce = require('lodash.debounce');

const NavigatorArrow: React.FC = (): JSX.Element => {
  const { state, dispatch } = useContext(Context);
  const [icon, setIcon] = useState<any>('chevron-down');

  // show modal with message about finish "tour"
  const showModalKnowMeFinished = (): void => {
    dispatch({
      type: 'MODAL_UPDATE',
      value: {
        title: getMessageByPath(
          state.currentLanguage,
          'elements.modal.titleKnowMeFinished',
        ),
        content: KnowMeFinished,
        showFooter: false,
        visible: true,
      },
    });
  };

  // Change "currentSection"
  const setNextSection = debounce((): void => {
    const currentSectionIndex = state.sections.findIndex(
      (section: SectionsType) => {
        return section.id === state.currentSection;
      },
    );
    const indexNextSection =
      currentSectionIndex + 1 < state.sections.length
        ? currentSectionIndex + 1
        : 0;

    if (indexNextSection === 0) {
      showModalKnowMeFinished();
      setIcon('chevron-down');
    }

    if (indexNextSection + 1 === state.sections.length) {
      setIcon('chevron-up');
    }

    dispatch({
      type: 'SET_CURRENT_SECTION',
      value: state.sections[indexNextSection].id,
    });
  }, 250);

  return (
    <div className="navigator-arrow">
      <Button
        className="btn-circle"
        data-testid="navigation-arrow"
        onClick={setNextSection}
      >
        <FontAwesomeIcon icon={icon} className="btn-circle-icon" />
      </Button>
    </div>
  );
};

export default NavigatorArrow;
