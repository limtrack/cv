import React from 'react';
// Components - UI
import Badge from 'react-bootstrap/Badge';

type Props = {
  data: string[];
};

const TagsList: React.FC<Props> = ({ data }: Props): JSX.Element => {
  return (
    <div className="tags">
      {data.map((tag: any, index: number) => {
        return (
          <Badge key={index} variant="primary">
            {tag}
          </Badge>
        );
      })}
    </div>
  );
};

export default TagsList;
