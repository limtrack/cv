import React from 'react';
// Components
import SocialNetworks from './SocialNetworks';

const FloatingSocialNetworks: React.FC = (): JSX.Element => {
  return (
    <div className="floating-ssnn">
      <SocialNetworks />
    </div>
  );
};

export default FloatingSocialNetworks;
