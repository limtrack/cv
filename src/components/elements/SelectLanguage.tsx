import React, { useContext } from 'react';
// Context
import { Context } from '../../provider/components/Context';
// Components - UI
import Form from 'react-bootstrap/Form';

const SelectLanguage: React.FC = (): JSX.Element => {
  const { state, dispatch } = useContext(Context);
  const handlerChange = (event: any): void => {
    const options = event.currentTarget.options;
    const selectedOption = event.currentTarget.selectedIndex;
    // Change section
    dispatch({
      type: 'CHANGE_LANGUAGE',
      value: options[selectedOption].value,
    });
  };

  return (
    <div className="select-language">
      <Form.Group>
        <Form.Control
          data-testid="select-language"
          as="select"
          onChange={handlerChange}
          defaultValue={state.currentLanguage}
        >
          {state.languages.map((lang: any) => {
            return (
              <option key={lang.id} value={lang.id}>
                {lang.label}
              </option>
            );
          })}
        </Form.Control>
      </Form.Group>
    </div>
  );
};

export default SelectLanguage;
