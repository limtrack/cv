import React from 'react';
import { render } from '@testing-library/react';
import SectionTitles from '../SectionTitles';

describe('Testing SectionTitles component', () => {
  it('render section title and subtitle', () => {
    const { getByText } = render(
      <SectionTitles title="Title  text" subtitle="Subtitle text" />,
    );

    expect(getByText(/Title text/)).toBeInTheDocument();
    expect(getByText(/Subtitle text/)).toBeInTheDocument();
  });

  it('render section title without subtitle', () => {
    const { getByText, queryByTestId } = render(
      <SectionTitles title="Title  text" />,
    );

    expect(getByText(/Title text/)).toBeInTheDocument();
    expect(queryByTestId('subtitle')).toBeNull();
  });

  it('render section subtitle without title', () => {
    const { getByText, queryByTestId } = render(
      <SectionTitles subtitle="Subtitle text" />,
    );

    expect(queryByTestId('title')).toBeNull();
    expect(getByText(/Subtitle text/)).toBeInTheDocument();
  });
});
