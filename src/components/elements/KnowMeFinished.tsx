import React, { useContext } from 'react';
// Context
import { Context } from '../../provider/components/Context';
// I18N
import { getMessageByPath } from '../../locale';

const KnowMeFinished: React.FC = (): JSX.Element => {
  const { state } = useContext(Context);
  // get messages from locale
  const messages = (key: string): string | any => {
    return getMessageByPath(
      state.currentLanguage,
      `elements.knowMeFinished.${key}`,
    );
  };

  return (
    <div className="know-me-finished">
      <div
        dangerouslySetInnerHTML={{
          __html: messages('text'),
        }}
      />
    </div>
  );
};

export default KnowMeFinished;
