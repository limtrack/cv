import React, { useContext } from 'react';
// Context
import { Context } from '../../provider/components/Context';
// Bootstrap
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

const CustomModal: React.FC = (): JSX.Element | null => {
  // state
  const { state, dispatch } = useContext(Context);
  const modal = state.modal;
  const Content = modal.content;

  /**
   * hide modal
   */
  const handleHide = (): void => {
    dispatch({
      type: 'MODAL_UPDATE',
      value: { visible: false },
    });
  };

  /**
   * reset modal
   */
  const handleExited = (): void => {
    dispatch({
      type: 'MODAL_RESET',
    });
  };

  /**
   * On click accept button
   */
  const onAcceptButton = (): void => {
    typeof modal.actionAcceptButton === 'function'
      ? modal.actionAcceptButton()
      : handleHide();
  };

  /**
   * On click cancel button
   */
  const onCancelButton = (): void => {
    typeof modal.actionCancelButton === 'function'
      ? modal.actionCancelButton()
      : handleHide();
  };

  return modal.content ? (
    <Modal
      centered
      backdrop={modal.backdrop}
      show={modal.visible}
      onHide={handleHide}
      onExited={handleExited}
    >
      {modal.showHeader ? (
        <>
          <Modal.Header closeButton={modal.showCloseButton}>
            <Modal.Title>{modal.title || 'Title modal'}</Modal.Title>
          </Modal.Header>
        </>
      ) : null}
      <Modal.Body>
        {typeof Content === 'string' ? Content : <Content />}
      </Modal.Body>
      {modal.showFooter ? (
        <>
          <Modal.Footer>
            {modal.showCancelButton ? (
              <Button variant="secondary" onClick={onCancelButton}>
                {modal.textCancelButton || 'Cancel'}
              </Button>
            ) : null}
            {modal.showAcceptButton ? (
              <Button variant="primary" onClick={onAcceptButton}>
                {modal.textAcceptButton || 'Accept'}
              </Button>
            ) : null}
          </Modal.Footer>
        </>
      ) : null}
    </Modal>
  ) : null;
};

export default CustomModal;
