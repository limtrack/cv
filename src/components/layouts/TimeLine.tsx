import React, { Fragment, useState } from 'react';
// Components - UI
import Badge from 'react-bootstrap/Badge';
import SectionTitles from '../elements/SectionTitles';
import TagsList from '../elements/TagsList';
import Form from 'react-bootstrap/Form';

// Types
type Props = {
  name?: string;
  title?: string;
  subtitle?: string;
  data: {
    date: string;
    title: string;
    subtitle: string;
    description: string;
    tags: string[];
  }[];
};

const TimeLine: React.FC<Props> = ({
  name = '',
  title,
  subtitle,
  data,
}: Props): JSX.Element => {
  const [dataIndex, setDataIndex] = useState(0);

  const handlerChange = (event: any): void => {
    setDataIndex(event.currentTarget.selectedIndex || 0);
  };

  return (
    <div className="section">
      <div className={`section-content timeline ${name}`}>
        <div className="timeline__top">
          <SectionTitles title={title} subtitle={subtitle} />
        </div>
        <div className="timeline__bottom">
          <div className="timeline__bottom__left">
            {/* Selector date */}
            <Form.Group className="timeline__selector appear-and-translateY__afterload-section">
              <Form.Control
                as="select"
                onChange={handlerChange}
                defaultValue="0"
              >
                {data.map((currentData: any, index: number) => {
                  return (
                    <option key={currentData.date} value={index}>
                      {currentData.date}
                    </option>
                  );
                })}
              </Form.Control>
            </Form.Group>
            {/* Description job */}
            <div className="timeline__description white-box appear-and-translateY__afterload-section">
              {dataIndex > -1 ? (
                <Fragment>
                  <h4 className="white-box__title">{data[dataIndex].title}</h4>
                  <h5 className="white-box__subtitle">
                    {data[dataIndex].subtitle}
                  </h5>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: data[dataIndex].description,
                    }}
                  />
                  <TagsList data={data[dataIndex].tags} />
                </Fragment>
              ) : (
                'You must select at least one date'
              )}
            </div>
          </div>
          <div className="timeline__bottom__right">
            {/* arrow */}
            <div className="arrow appear-and-translateY__afterload-section">
              <div className="point"></div>
              <div className="line"></div>
              <div className="circle"></div>
            </div>
            {/* timeline */}
            <div className="timeline-data appear-and-translateY__afterload-section-with-delay">
              {data.map((currentData: any, index: number) => {
                return (
                  <Badge
                    key={index}
                    pill
                    onClick={setDataIndex.bind(null, index)}
                    className={index === dataIndex ? 'focus' : ''}
                  >
                    {currentData.date}
                  </Badge>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TimeLine;
