import React, { Fragment, useState } from 'react';
// Components - UI
import SectionTitles from '../elements/SectionTitles';
import TagsList from '../elements/TagsList';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

type Props = {
  name?: string;
  title?: string;
  subtitle?: string;
  data: {
    name: string;
    description: string;
    img: string;
    tags: string[];
    url?: string;
    code?: string;
  }[];
};

const Gallery: React.FC<Props> = ({
  name = '',
  title,
  subtitle,
  data,
}: Props): JSX.Element => {
  // State
  const [dataIndex, setDataIndex] = useState(0);
  // Move to next or previous item inside gallery
  const goToItem = (step: number): void => {
    let item = dataIndex + step;

    if (item > data.length - 1) {
      item = 0;
    } else if (item < 0) {
      item = data.length - 1;
    }

    setDataIndex(item);
  };
  const goPrev = (): void => {
    goToItem(-1);
  };
  const goNext = (): void => {
    goToItem(1);
  };

  return (
    <div className="section">
      <div className={`section-content gallery ${name}`}>
        <div className="gallery__top">
          <SectionTitles title={title} subtitle={subtitle} />
        </div>
        <div className="gallery__bottom">
          <div className="white-box appear-and-translateY__afterload-section">
            {/* Navigation */}
            <Button
              variant="secondary"
              className="btn-circle gallery_prev"
              onClick={goPrev}
            >
              <FontAwesomeIcon
                icon="chevron-left"
                className="btn-circle-icon"
              />
            </Button>
            <Button
              variant="secondary"
              className="btn-circle gallery_next"
              onClick={goNext}
            >
              <FontAwesomeIcon
                icon="chevron-right"
                className="btn-circle-icon"
              />
            </Button>
            {/* Gallery */}
            <div className="gallery__bottom__left">
              <div className="gallery__description">
                {dataIndex > -1 ? (
                  <Fragment>
                    <h4 className="gallery__description__name">
                      {data[dataIndex].name}
                    </h4>
                    <div
                      dangerouslySetInnerHTML={{
                        __html: data[dataIndex].description,
                      }}
                    />
                    <div className="gallery__actions">
                      <Button variant="primary" href={data[dataIndex].url}>
                        URL
                      </Button>
                      <Button
                        variant="outline-primary"
                        href={data[dataIndex].code}
                      >
                        Code
                      </Button>
                    </div>
                    <TagsList data={data[dataIndex].tags} />
                  </Fragment>
                ) : (
                  'You must have at least one item'
                )}
              </div>
            </div>
            <div className="gallery__bottom__right">
              <div
                style={{ backgroundImage: `url('${data[dataIndex].img}')` }}
                className="gallery__image"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Gallery;
