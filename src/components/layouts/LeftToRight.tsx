import React from 'react';
// Components
import SectionTitles from '../elements/SectionTitles';

type Props = {
  name?: string;
  title?: string;
  subtitle?: string;
  content: JSX.Element;
};

const LeftToRIght: React.FC<Props> = ({
  name = '',
  title,
  subtitle,
  content,
}: Props): JSX.Element => {
  return (
    <div className="section">
      <div className={`section-content left-to-right ${name}`}>
        <div className="left-to-right__left">
          <SectionTitles title={title} subtitle={subtitle} />
        </div>
        <div className="left-to-right__right">{content}</div>
      </div>
    </div>
  );
};

export default LeftToRIght;
