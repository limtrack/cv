/// <reference types="cypress" />

context('All behaviours in the navigation', () => {
  const navigationArrowId = '#navigation-arrow';
  const sections = [
    'information',
    'experience',
    'studies',
    'works',
    'others',
    'contact',
  ];
  const waitingScroll = 2000;

  it(`Walking all sections and final modal`, () => {
    cy.visit('/');

    const doScrolltoSectionAndCheck = (sectionIndex) => {
      // click
      cy.get(navigationArrowId).click();
      // waiting to scroll
      cy.wait(waitingScroll);
      // we find new section
      cy.get(`[name="${sections[sectionIndex]}"] > .section`).should(
        'be.visible',
      );

      if (sectionIndex < sections.length - 1) {
        doScrolltoSectionAndCheck(sectionIndex + 1);
      } else {
        // Check final modal
        cy.get(`.modal.show`).should('be.visible');
      }
    };

    // Start the check of all sections
    doScrolltoSectionAndCheck(0);
  });
});
